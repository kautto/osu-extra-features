<?php
/**
 * @file
 * osu_publications.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function osu_publications_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'osu_publications';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'OSU Publications';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Publications';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '100';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['wrapper_class'] = 'publication-list';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Authors */
  $handler->display->display_options['fields']['field_osu_authors']['id'] = 'field_osu_authors';
  $handler->display->display_options['fields']['field_osu_authors']['table'] = 'field_data_field_osu_authors';
  $handler->display->display_options['fields']['field_osu_authors']['field'] = 'field_osu_authors';
  $handler->display->display_options['fields']['field_osu_authors']['label'] = '';
  $handler->display->display_options['fields']['field_osu_authors']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_osu_authors']['element_label_colon'] = FALSE;
  /* Field: Content: File */
  $handler->display->display_options['fields']['field_osu_file']['id'] = 'field_osu_file';
  $handler->display->display_options['fields']['field_osu_file']['table'] = 'field_data_field_osu_file';
  $handler->display->display_options['fields']['field_osu_file']['field'] = 'field_osu_file';
  $handler->display->display_options['fields']['field_osu_file']['label'] = '';
  $handler->display->display_options['fields']['field_osu_file']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_osu_file']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_osu_file']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_osu_file']['type'] = 'file_download_link';
  $handler->display->display_options['fields']['field_osu_file']['settings'] = array(
    'text' => 'Download [file:name]',
  );
  /* Field: Content: Publication Date */
  $handler->display->display_options['fields']['field_osu_pubdate']['id'] = 'field_osu_pubdate';
  $handler->display->display_options['fields']['field_osu_pubdate']['table'] = 'field_data_field_osu_pubdate';
  $handler->display->display_options['fields']['field_osu_pubdate']['field'] = 'field_osu_pubdate';
  $handler->display->display_options['fields']['field_osu_pubdate']['label'] = '';
  $handler->display->display_options['fields']['field_osu_pubdate']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_osu_pubdate']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_osu_pubdate']['alter']['text'] = '([field_osu_pubdate])';
  $handler->display->display_options['fields']['field_osu_pubdate']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_osu_pubdate']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_osu_pubdate']['settings'] = array(
    'format_type' => 'panopoly_day',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Reference */
  $handler->display->display_options['fields']['field_osu_reference']['id'] = 'field_osu_reference';
  $handler->display->display_options['fields']['field_osu_reference']['table'] = 'field_data_field_osu_reference';
  $handler->display->display_options['fields']['field_osu_reference']['field'] = 'field_osu_reference';
  $handler->display->display_options['fields']['field_osu_reference']['label'] = '';
  $handler->display->display_options['fields']['field_osu_reference']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_osu_reference']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_osu_reference']['type'] = 'text_plain';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '[title]
<span class="authors">[field_osu_authors]</span>
<span class="reference">[field_osu_reference] <span class="date">[field_osu_pubdate]</span></span>
[field_osu_file] ';
  $handler->display->display_options['fields']['nothing']['alter']['nl2br'] = TRUE;
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Publication Date (field_osu_pubdate) */
  $handler->display->display_options['sorts']['field_osu_pubdate_value']['id'] = 'field_osu_pubdate_value';
  $handler->display->display_options['sorts']['field_osu_pubdate_value']['table'] = 'field_data_field_osu_pubdate';
  $handler->display->display_options['sorts']['field_osu_pubdate_value']['field'] = 'field_osu_pubdate_value';
  $handler->display->display_options['sorts']['field_osu_pubdate_value']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'publication' => 'publication',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'publications';
  $export['osu_publications'] = $view;

  return $export;
}
