<?php
/**
 * @file
 * osu_gallery.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function osu_gallery_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function osu_gallery_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function osu_gallery_image_default_styles() {
  $styles = array();

  // Exported image style: gallery_crop_3x4.
  $styles['osu_gallery_crop_3x4'] = array(
    'name' => 'osu_gallery_crop_3x4',
    'label' => 'Gallery Crop (3x4)',
    'effects' => array(
      2 => array(
        'label' => 'Manual Crop: Crop and scale',
        'help' => 'Crop and scale a user-selected area, respecting the ratio of the destination width and height.',
        'effect callback' => 'manualcrop_crop_and_scale_effect',
        'form callback' => 'manualcrop_crop_and_scale_form',
        'summary theme' => 'manualcrop_crop_and_scale_summary',
        'module' => 'manualcrop',
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 768,
          'height' => 1024,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'osu_gallery_crop_3x4',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: gallery_crop_4x3.
  $styles['osu_gallery_crop_4x3'] = array(
    'name' => 'osu_gallery_crop_4x3',
    'label' => 'Gallery Crop (4x3)',
    'effects' => array(
      1 => array(
        'label' => 'Manual Crop: Crop and scale',
        'help' => 'Crop and scale a user-selected area, respecting the ratio of the destination width and height.',
        'effect callback' => 'manualcrop_crop_and_scale_effect',
        'form callback' => 'manualcrop_crop_and_scale_form',
        'summary theme' => 'manualcrop_crop_and_scale_summary',
        'module' => 'manualcrop',
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 1024,
          'height' => 768,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'osu_gallery_crop_4x3',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: gallery_thumbnail.
  $styles['osu_gallery_thumbnail'] = array(
    'name' => 'osu_gallery_thumbnail',
    'label' => 'Gallery Thumbnail',
    'effects' => array(
      7 => array(
        'label' => 'Manual Crop: Automatically reuse cropped style',
        'help' => 'Load the first applied crop selection and reuse it.',
        'effect callback' => 'manualcrop_auto_reuse_effect',
        'form callback' => 'manualcrop_auto_reuse_form',
        'summary theme' => 'manualcrop_auto_reuse_summary',
        'module' => 'manualcrop',
        'name' => 'manualcrop_auto_reuse',
        'data' => array(
          'style_priority' => array(
            0 => 'osu_gallery_crop_4x3',
            1 => 'osu_gallery_crop_3x4',
          ),
          'fallback_style' => '',
        ),
        'weight' => 0,
      ),
      8 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => '',
          'height' => 120,
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: gallery_thumbnail_3x4.
  $styles['osu_gallery_thumbnail_3x4'] = array(
    'name' => 'osu_gallery_thumbnail_3x4',
    'label' => 'Gallery Thumbnail (3x4)',
    'effects' => array(
      5 => array(
        'label' => 'Manual Crop: Reuse cropped style',
        'help' => 'Reuse a crop selection from another Manual Crop enabled image style.',
        'effect callback' => 'manualcrop_reuse_effect',
        'form callback' => 'manualcrop_reuse_form',
        'summary theme' => 'manualcrop_reuse_summary',
        'module' => 'manualcrop',
        'name' => 'manualcrop_reuse',
        'data' => array(
          'reuse_crop_style' => 'osu_gallery_crop_3x4',
        ),
        'weight' => 0,
      ),
      6 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 120,
          'height' => 160,
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: gallery_thumbnail_4x3.
  $styles['osu_gallery_thumbnail_4x3'] = array(
    'name' => 'osu_gallery_thumbnail_4x3',
    'label' => 'Gallery Thumbnail (4x3)',
    'effects' => array(
      3 => array(
        'label' => 'Manual Crop: Reuse cropped style',
        'help' => 'Reuse a crop selection from another Manual Crop enabled image style.',
        'effect callback' => 'manualcrop_reuse_effect',
        'form callback' => 'manualcrop_reuse_form',
        'summary theme' => 'manualcrop_reuse_summary',
        'module' => 'manualcrop',
        'name' => 'manualcrop_reuse',
        'data' => array(
          'reuse_crop_style' => 'osu_gallery_crop_4x3',
        ),
        'weight' => 0,
      ),
      4 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 160,
          'height' => 120,
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function osu_gallery_node_info() {
  $items = array(
    'picture' => array(
      'name' => t('Picture'),
      'base' => 'node_content',
      'description' => t('Basic content type for pictures.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
